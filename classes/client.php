<?php
/**
 *
 *
 * @package   GoogleTranslate
 * @version   1.0.0
 * @author    Kazunori Ninomiya (2no)
 * @license   MIT License
 * @copyright 2014 2no
 */

namespace GoogleTranslate;

class Client
{
	/**
	 * end point uri
	 *
	 * @var string
	 */
	const END_POINT = 'https://www.googleapis.com/language/translate/v2';

	/**
	 * @var int
	 */
	const DEFAULT_TIMEOUT = 5000;

	/**
	 * cURL default option
	 *
	 * @var array
	 */
	protected static $_default_options = array(
		CURLOPT_POST           => true,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPHEADER     => array('X-HTTP-Method-Override: GET'),
	);

	/**
	 * API key
	 *
	 * @var string
	 */
	protected $_key;

	/**
	 * end point
	 *
	 * @var string
	 */
	protected $_end_point;

	/**
	 * @var int
	 */
	protected $_timeout;

	/**
	 * 初期化します。
	 *
	 * 一番最初に固有の設定を読み込みます。
	 */
	public static function _init()
	{
		\Config::load('googletranslate', 'googletranslate');
	}

	/**
	 *
	 * @param  string|null $key API key
	 * @return GoogleTranslate\Client
	 */
	public static function forge($key = null)
	{
		return new static($key);
	}

	/**
	 * 新たにインスタンスを生成します。
	 *
	 * @param string|null $key デベロッパキー
	 */
	public function __construct($key = null)
	{
		$this->_end_point = static::END_POINT;
		$this->_timeout   = static::DEFAULT_TIMEOUT;
		$this->_key       = $key ?: \Config::get('googletranslate.key');

		$this->set_timeout(\Config::get('googletranslate.timeout'));
	}

	/**
	 * @param string $name
	 * @param mixed  $value
	 */
	public function __set($name, $value)
	{
		$method = 'set_'.$name;
		if (is_callable(array($this, $method)))
		{
			$this->$method($value);
		}
	}

	/**
	 * @param  string $name
	 * @return mixed
	 */
	public function __get($name)
	{
		$method = 'get_'.$name;
		if (is_callable(array($this, $method)))
		{
			return $this->$method();
		}
	}

	/**
	 * デベロッパキーを設定します。
	 *
	 * @param  string $key
	 * @return GoogleTranslate\Client 自身のインスタンス
	 */
	public function set_key($key)
	{
		if (is_string($key))
		{
			$this->_key = $key;
		}

		return $this;
	}

	/**
	 * デベロッパキーを取得します。
	 *
	 * @return string
	 */
	public function get_key()
	{
		return $this->_key;
	}

	/**
	 * @param  string $end_point
	 * @return GoogleTranslate\Client 自身のインスタンス
	 */
	public function set_end_point($end_point)
	{
		if (is_string($end_point))
		{
			$this->_end_point = rtrim($end_point, " \t\n\r\0\x0B\/");
		}

		return $this;
	}

	/**
	 * @return string
	 */
	public function get_end_point()
	{
		return $this->_end_point;
	}

	/**
	 *
	 * @param  int $timeout
	 * @return GoogleTranslate\Client 自身のインスタンス
	 */
	public function set_timeout($timeout)
	{
		if (is_numeric($timeout))
		{
			$this->_timeout = $timeout < 0 ? 1 : (int)$timeout;
		}

		return $this;
	}

	/**
	 * @return int
	 */
	public function get_timeout()
	{
		$this->_timeout;
	}

	/**
	 * 翻訳します。
	 *
	 * @param  string|array $text
	 * @param  string       $target
	 * @param  string|null  $source
	 * @param  bool         $prettyprint
	 * @return array 翻訳結果
	 * @throws GoogleTranslate\Exception
	 */
	public function translate(
		$text, $target, $source = null, $prettyprint = true)
	{
		$data = $this->_http_build_query(array(
			'q'           => $text,
			'target'      => is_string($target) ? $target : null,
			'source'      => is_string($source) ? $source : null,
			'prettyprint' => (bool)$prettyprint,
		));
		$result = $this->_request($this->_end_point, $data);
		return $result->translations;
	}

	/**
	 * Detect the language of text.
	 * 
	 * Typical usage is:
	 * <code>
	 *   $client = GoogleTranslate\Client::forge();
	 *   $client->detect('example');
	 * </code>
	 *
	 * @param  string|array $text 対象文字列
	 * @return array 判定した言語
	 * @throws GoogleTranslate\Exception
	 */
	public function detect($text)
	{
		$data   = $this->_http_build_query(array('q' => $text));
		$result = $this->_request($this->_end_point.'/detect', $data);
		return $result->detections;
	}

	/**
	 * サポートしている言語を取得します。
	 *
	 * @param  string|null $target
	 * @return array サポート言語
	 * @throws GoogleTranslate\Exception
	 */
	public function supported_languages($target = null)
	{
		$data   = array('target' => is_string($target) ? $target : null);
		$data   = $this->_http_build_query($data);
		$result = $this->_request($this->_end_point.'/languages', $data);
		return $result->languages;
	}

	/**
	 * リクエストを送信します。
	 * 
	 * @param  string $url  送信先
	 * @param  string $data クエリストリング
	 * @return object レスポンスデータ
	 */
	protected function _request($url, $data)
	{
		$options = static::$_default_options + array(
			CURLOPT_POSTFIELDS => $data,
			CURLOPT_TIMEOUT_MS => $this->_timeout,
		);
		$ch = curl_init($url);
		curl_setopt_array($ch, $options);
		$response = curl_exec($ch);
		$code     = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$error    = curl_error($ch);
		curl_close($ch);

		if (400 <= $code || 0 === $code)
		{
			throw new Exception($error, $code);
		}

		$response = json_decode($response);
		if (isset($response->error))
		{
			$error = $response->error;
			throw new Exception($error->message, $error->code, null, $error->errors);
		}

		return $response->data;
	}

	private function _http_build_query(array $query_data)
	{
		$data = array();
		$query_data['key'] = $this->_key;

		if (isset($query_data['q']))
		{
			if (is_string($query_data['q']))
			{
				$query_data['q'] = array($query_data['q']);
			}

			if (is_array($query_data['q']))
			{
				foreach ($query_data['q'] as $text)
				{
					$data[] = http_build_query(array('q' => $text));
				}
			}

			unset($query_data['q']);
		}

		$data[] = http_build_query($query_data);
		return implode('&', $data);
	}
}

