<?php
/**
 *
 *
 * @package   GoogleTranslate
 * @version   1.0.0
 * @author    Kazunori Ninomiya (2no)
 * @license   MIT License
 * @copyright 2014 2no
 */

namespace GoogleTranslate;

class Exception extends \Exception
{
	/**
	 * @var array
	 */
	protected $_errors;

	/**
	 * @param string    $message
	 * @param int       $code
	 * @param Exception $previous
	 * @param array     $errors
	 */
	public function __construct(
		$message,
		$code = 0,
		\Exception $previous = null,
		array $errors = array())
	{
		parent::__construct($message, $code, $previous);
		$this->_errors = $errors;
	}

	/**
	 * @return array
	 */
	public function get_errors()
	{
		return $this->_errors;
	}
}
