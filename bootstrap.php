<?php
/**
 * GoogleTranslate: 
 *
 * @package   Fuel
 * @version   1.0.0
 * @author    Kazunori Ninomiya (2no)
 * @copyright 2014 2no
 */

Autoloader::add_core_namespace('GoogleTranslate');

Autoloader::add_classes(array(
	'GoogleTranslate\\Client'    => __DIR__.'/classes/client.php',
	'GoogleTranslate\\Exception' => __DIR__.'/classes/exception.php',
));

